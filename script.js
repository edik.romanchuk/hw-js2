let userName = prompt('Як тебе звати?');

while (!userName) {
    userName = prompt('Як тебе звати?');
}

let age = prompt('Скільки тобі років?');

while (!Number(age)) {
    age = prompt('Скільки тобі років?');
}

if (age < 18) {
    alert('You are not allowed to visit this website');

}

if (age >= 18 && age <= 22) {
    const needToContinue = confirm('Are you sure you want to continue?');

    if (needToContinue) {
        alert('Welcome, ' + userName);
    } else {
        alert('You are not allowed to visit this website');
    }

} 

if (age > 22) {
    alert('Welcome, ' + userName);
}